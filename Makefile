COMPOSE_PATH = docker/docker-compose.yaml
COMPOSE_ENV = docker-compose -f ${COMPOSE_PATH}
COMPOSE_RUN = ${COMPOSE_ENV} run --rm
COMPOSE_EXEC = ${COMPOSE_ENV} exec

WEB_SERVICE = web
WEB_RUN = ${COMPOSE_RUN} ${WEB_SERVICE}
WEB_EXEC = ${COMPOSE_EXEC} ${WEB_SERVICE}

DB_SERVICE = web
DB_RUN = ${COMPOSE_RUN} ${DB_SERVICE}
DB_EXEC = ${COMPOSE_EXEC} ${DB_SERVICE}

.PHONY: _test test

##+MAKEFILE HELPER
##+
##+GENERAL COMMANDS
help: 				##+ Lista os comandos disponíveis.
	@fgrep -h "##+" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##+//'

##+
##+COMPOSE CMD
up:			##+ Builds the compose containers.
	${COMPOSE_ENV} up -d
down:
	${COMPOSE_ENV} down

build:			##+ Builds the compose containers.
	docker-compose -f docker/docker-compose.yaml build
##+
##+DEVELOPMENT SERVER
bash:			##+ Sobe um ambiente de desenvolvimento (DB + server) e abre um terminal no server.
	${WEB_EXEC} bash
rails c:			##+ Sobe um ambiente de desenvolvimento (DB + server) e abre um terminal no server.
	${WEB_EXEC} rails c

##+
##+RAILS RELATED
db-create: 				##+ todo
	${WEB_EXEC} rake db:create

db-migrate: 				##+ todo
	${WEB_EXEC} rake db:migrate

rspec: 				##+ Roda testes rspec em ambiente docker.
	${WEB_EXEC} make _rspec

_rspec:				##+ Roda testes rspec no ambiente local.
	bundle exec rspec

rubocop:			##+ Roda testes rubocop em ambiente docker.
	${WEB_EXEC} make _rubocop

_rubocop:			##+ Roda testes rubocop no ambiente local.
	bundle exec rubocop

test:				##+ Roda todos os testes em ambiente docker.
	${WEB_EXEC} make _test
.PHONY: test

_test:				##+ Roda todos os testes no ambiente local.
	_rspec && _rubocop

##+