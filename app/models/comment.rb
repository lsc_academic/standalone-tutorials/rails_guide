class Comment < ApplicationRecord
  belongs_to :article
  validates :title, presence: true,
                    length: { maximum: 5 }
end
